package INF102.lab2.list;

public class LinkedList<T> implements List<T> {

	private int n;
	
	/**
	 * If list is empty, head == null
	 * else head is the first element of the list.
	 */
	private Node<T> head;

	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}

	@Override
	public T get(int index) {
		return getNode(index).data;
	}
	
	/**
     * Returns the node at the specified position in this list.
     *
     * @param index index of the node to return
     * @return the node at the specified position in this list
     * @throws IndexOutOfBoundsException if the index is out of range
     *         ({@code index < 0 || index >= size()})
     */
	private Node<T> getNode(int index) {
		// TODO: Implement method
		if(index < 0 || index >= this.size() || isEmpty()){
			throw new IndexOutOfBoundsException();
		}
		Node<T> cur_node = this.head;
		for(int i = 0; i < index; i++){
			cur_node = cur_node.next;
		}
		return cur_node;
	}

	@Override
	public void add(int index, T element) {
		this.n++;
		if(index < 0 || index >= this.size()){
			throw new IndexOutOfBoundsException();
		}
		Node<T> new_node = new Node<T>(element);
		if(this.head==null){
			this.head= new_node;
		}
		else if(index==0){
			new_node.next = this.head;
			this.head = new_node;
		}
		else{
			Node<T> node_before_index = getNode(index-1);
			new_node.next = node_before_index.next;
			node_before_index.next = new_node;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		Node<T> currentNode = head;
		while (currentNode.next != null) {
			str.append(currentNode.data);
			str.append(", ");
			currentNode = currentNode.next;
		}
		str.append((T) currentNode.data);
		str.append("]");
		return str.toString();
	}

	@SuppressWarnings("hiding")
	private class Node<T> {
		T data;
		Node<T> next;

		public Node(T data) {
			this.data = data;
		}
	}
	
}