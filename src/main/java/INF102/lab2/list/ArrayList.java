package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {

	
	public static final int DEFAULT_CAPACITY = 10;
	
	private int n;
	
	private Object elements[];
	
	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}
		
	@Override
	public T get(int index) {
		if(index < 0 || index >= this.size() || isEmpty()){
			throw new IndexOutOfBoundsException();
		}
		
		return (T)this.elements[index];
	}
	

	@Override
	public void add(int index, T element) {
		this.n++;
		if(this.size() >= this.elements.length){
			Object elements_old[] = this.elements.clone();
			this.elements = Arrays.copyOf(elements_old, elements_old.length*2);
		}

		for(int i = this.size()-1; i >= index; i--){
			T tmp_elem = (T)this.elements[i];
			this.elements[i+1] = tmp_elem;
		}
		this.elements[index] = element;
	}
	
	//Does not fully pass tests, but is smart idea
	//@Override
	//public void add(int index, T element) {
	//	this.n++;
	//	if(this.size() >= this.elements.length){
	//		Object elements_old[] = this.elements.clone();
	//		this.elements = Arrays.copyOf(elements_old, elements_old.length*2);
	//	}
//
	//	this.elements[this.size()] = this.elements[index];
	//	this.elements[index] = element;
	//}
	
	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n-1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

}